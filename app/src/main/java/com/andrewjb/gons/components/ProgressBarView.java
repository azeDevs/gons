package com.andrewjb.gons.components;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

import com.andrewjb.gons.R;

/**
 * Class type ProgressBarView.
 */
public class ProgressBarView extends View {

    public static final String TAG = "ProgressBarView";


    private Paint paintFill, paintBar, paintStroke;
    private float progress;
    private float animatedProgress;

    /**
     * Instantiates a new Progress bar view.
     *
     * @param context the context
     */
    public ProgressBarView(Context context) {
        super(context);
    }

    /**
     * Instantiates a new Progress bar view.
     *
     * @param context the context
     * @param attrs   the attrs
     */
    public ProgressBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);

    }

    /**
     * Instantiates a new Progress bar view.
     *
     * @param context      the context
     * @param attrs        the attrs
     * @param defStyleAttr the def style attr
     */
    public ProgressBarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ProgressBarView, 0, 0);
        float attrText = a.getFloat(R.styleable.ProgressBarView_progress, 0);
        setProgress(attrText);
        a.recycle();

        makePaints();
        animateProgress();
    }

    /**
     * Sets progress.
     *
     * @param progress the progress
     */
    public void setProgress(float progress) {
        this.progress = progress;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        final RectF rectBar = new RectF(0, 0, getWidth(), getHeight());
        final RectF rectFill = new RectF(0, 0, getWidth() * animatedProgress, getHeight());

        canvas.drawRect(rectBar, paintBar);
        canvas.drawRect(rectFill, paintFill);
        canvas.drawRect(rectBar, paintStroke);
    }

    private void makePaints() {
        paintFill = new Paint();
        paintBar = new Paint();
        paintStroke = new Paint();

        paintBar.setColor(getResources().getColor(R.color.grd0));
        paintBar.setStyle(Paint.Style.FILL);

        paintFill.setColor(getResources().getColor(R.color.grd2));
        paintFill.setStyle(Paint.Style.FILL);

        paintStroke.setColor(getResources().getColor(R.color.grd2));
        paintStroke.setStrokeWidth(6f);
        paintStroke.setStyle(Paint.Style.STROKE);
    }

    private void animateProgress() {
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, progress);
        valueAnimator.setDuration(1000);
        valueAnimator.setStartDelay(400);
        valueAnimator.setInterpolator(new DecelerateInterpolator(3f));
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                animatedProgress = (float) animation.getAnimatedValue();
                invalidate();
            }
        });

        valueAnimator.start();
    }
}
