package com.andrewjb.gons.objects;

import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Class type ObservableCellList.
 */
public class CellGrid extends ArrayList<Cell> {

    public static final String TAG = "RenderManager";

    public CellGrid(CellGrid copy) {
        this.addAll(copy);
    }

    public CellGrid() {
    }

    /**
     * Gets adjacent.
     *
     * @param coord     the coord
     * @param direction the direction
     * @return the adjacent
     */
    public Cell getAdjacent(CubeCoord coord, CubeCoord direction) {
        int targetX = coord.x + direction.x;
        int targetY = coord.y + direction.y;
        int targetZ = coord.z + direction.z;

        for (int i = 0; i < size(); i++) {
            int cursorX = get(i).getCubeCoord().x;
            int cursorY = get(i).getCubeCoord().y;
            int cursorZ = get(i).getCubeCoord().z;

            if (cursorX == targetX && cursorY == targetY && cursorZ == targetZ)
                return get(i);
        }

        return null;
    }

    /**
     * Gets adjacent power.
     *
     * @param coord the coord
     * @return the adjacent power
     */
    public Mutation getMutation(CubeCoord coord) {
        int redPower = 0;
        int grnPower = 0;
        for (int adjacency = 0; adjacency < 6; adjacency++) {
            CubeCoord target = scanAdjacent(coord, adjacency);

            for (int i = 0; i < size(); i++) {
                CubeCoord cursor = get(i).getCubeCoord();

                if (cursor.hasSameCoordsAs(target))
                    switch (get(i).getTeam()) {
                        case Cell.TEAM_RED:
                            redPower += get(i).getPower();
                            break;
                        case Cell.TEAM_GRN:
                            grnPower += get(i).getPower();
                            break;
                    }
            }
        }
        if (redPower > grnPower) {
            Log.i(TAG, "red");
            return new Mutation(redPower, Cell.TEAM_RED);
        }
        else if (grnPower > redPower) {
            Log.i(TAG, "green");
            return new Mutation(grnPower, Cell.TEAM_GRN);
        }
        else {
            Log.i(TAG, "nil");
            return new Mutation(0, Cell.TEAM_NIL);
        }
    }

    public int getAdjacentTeam(CubeCoord coord) {
        int redTeam = 0;
        int grnTeam = 0;
        for (int adjacency = 0; adjacency < 6; adjacency++) {
            CubeCoord target = scanAdjacent(coord, adjacency);

            for (int i = 0; i < size(); i++) {
                CubeCoord cursor = get(i).getCubeCoord();

                if (cursor.hasSameCoordsAs(target))
                    switch (get(i).getTeam()) {
                        case Cell.TEAM_RED:
                            redTeam++;
                            break;
                        case Cell.TEAM_GRN:
                            grnTeam++;
                            break;
                    }
            }
        }
        if (redTeam > grnTeam)
            return Cell.TEAM_RED;
        else if (grnTeam > redTeam)
            return Cell.TEAM_GRN;

        return Cell.TEAM_NIL;
    }

    /**
     * Add cell.
     *
     * @param cell the cell
     */
    public void addCell(Cell cell) {
        this.add(cell);
    }

    /**
     * Add cell.
     *
     * @param c    the c
     * @param team the team
     */
    public void addCell(CubeCoord c, int team) {
        this.add(new Cell(c, team));
    }

    /**
     * Add cell.
     *
     * @param c     the c
     * @param power the power
     * @param team  the team
     */
    public void addCell(CubeCoord c, int power, int team) {
        this.add(new Cell(new CubeCoord(c), power, team));
    }


    public Cell getCell(CubeCoord coord) {
        for (int i = 0; i < size(); i++) {
            if (get(i).getCubeCoord().hasSameCoordsAs(coord))
                return get(i);
        }
        return new Cell(coord, -1, Cell.TEAM_NIL);
    }

    public CubeCoord scanAdjacent(CubeCoord coord, int i) {
        CubeCoord newCoord = new CubeCoord(coord);
        CubeCoord direction = new CubeCoord();
        switch(i) {
            case 0: direction.setSameAs(CubeCoord.UP_RIGHT); break;
            case 1: direction.setSameAs(CubeCoord.RIGHT); break;
            case 2: direction.setSameAs(CubeCoord.DN_RIGHT); break;
            case 3: direction.setSameAs(CubeCoord.DN_LEFT); break;
            case 4: direction.setSameAs(CubeCoord.LEFT); break;
            case 5: direction.setSameAs(CubeCoord.UP_LEFT); break;
        }
        newCoord.translate(direction);
        return newCoord;
    }

    public void clean() {
        Iterator<Cell> iterator = iterator();
        Cell cell;
        while (iterator.hasNext()) {
            cell = iterator.next();
            if (cell.getPower() <= 0)
                iterator.remove();
            else if (cell.getPower() > 3)
                iterator.remove();
        }
    }

    public void generate() {
        clean();
        createVacancies();
        powerUp();
        clean();
        createVacancies();
    }

    public void createVacancies() {
        // scan grid for live cell
        CubeCoord cursor = new CubeCoord();
        CubeCoord target = new CubeCoord();
        for (int i = 0; i < size(); i++) {
            if (get(i).getPower() > 0) {
                // scan surrounding for empty space and place vacancy
                for (int adjacency = 0; adjacency < 6; adjacency++) {

                    cursor.setSameAs(get(i).getCubeCoord());
                    target.setSameAs(scanAdjacent(cursor, adjacency));

                    if (getCell(target).getPower() < 0) {
                        addCell(target, 0, Cell.TEAM_NIL);
                    }
                }
            }
        }
    }

    private void powerUp() {
        CellGrid backup = new CellGrid(this);
        for (int i = 0; i < size(); i++) {
            Mutation mutation = getMutation(backup.get(i).getCubeCoord());
            switch (mutation.power) {
                case 0:
                    mutateCell(backup, i, Cell.TEAM_NIL, -2, -2);
                    break;
                case 1:
                    mutateCell(backup, i, mutation.team, -1, -1);
                    break;
                case 2:
                    mutateCell(backup, i, mutation.team, -1, -1);
                    break;
                case 3:
                    mutateCell(backup, i, mutation.team, -2, 0);
                    break;
                case 4:
                    mutateCell(backup, i, mutation.team, -2, 0);
                    break;
                case 5:
                    mutateCell(backup, i, mutation.team, -3, 0);
                    break;
                case 6:
                    mutateCell(backup, i, mutation.team, -3, 1);
                    break;
                case 7:
                    mutateCell(backup, i, mutation.team, -3, 1);
                    break;
                case 8:
                    mutateCell(backup, i, mutation.team, -3, 1);
                    break;
                case 9:
                    mutateCell(backup, i, mutation.team, -3, 2);
                    break;
                case 10:
                    mutateCell(backup, i, mutation.team, -3, 2);
                    break;
                case 11:
                    mutateCell(backup, i, mutation.team, -3, 2);
                    break;
                case 12:
                    mutateCell(backup, i, mutation.team, -3, 2);
                    break;
                default:
                    if (mutation.power > 12) {
                        mutateCell(backup, i, mutation.team, -4, 3);
                    } else if (mutation.power < 0) {
                        mutateCell(backup, i, Cell.TEAM_NIL, -2, -2);
                    }
                    break;
            }
        }
    }

    private void mutateCell(CellGrid backup, int i, int adjacentTeam, int badTouch, int goodTouch) {
        if (backup.get(i).getPower() > 0) {
            adjacentTeam = backup.get(i).getTeam();
        }


        if (backup.get(i).getTeam() == backup.getMutation(backup.get(i).getCubeCoord()).team || backup.get(i).getTeam() == Cell.TEAM_NIL) {
            get(i).changePower(goodTouch);
        }
        else {
            get(i).changePower(badTouch);
        }
        get(i).setTeam(adjacentTeam);
    }

}
