package com.andrewjb.gons.objects;

/**
 * Class type Cell.
 */
public class Cell {

    public static final int TEAM_NIL = 0x000;
    public static final int TEAM_RED = 0xF00;
    public static final int TEAM_GRN = 0x0F0;


    public CubeCoord c;
    private int power;
    private int team;
    private boolean markOfDeath;

    /**
     * Instantiates a new Cell.
     *
     * @param cell the cell
     */
    public Cell(Cell cell) {
        this(cell.c, cell.power, cell.team);
    }

    /**
     * Instantiates a new Cell.
     *
     * @param c    the c
     * @param team the team
     */
    public Cell(CubeCoord c, int team) {
        this.c = c;
        this.power = 1;
        this.team = team;
    }

    /**
     * Instantiates a new Cell.
     *
     * @param c     the c
     * @param power the power
     * @param team  the team
     */
    public Cell(CubeCoord c, int power, int team) {
        this.c = c;
        this.power = power;
        this.team = team;
    }

    public CubeCoord getCubeCoord() {
        return this.c;
    }

    /**
     * Gets power.
     *
     * @return the power
     */
    public int getPower() {
        return power;
    }

    /**
     * Sets power.
     *
     * @param power the power
     */
    public void setPower(int power) {
        this.power = power;
    }

    public void changePower(int power) {
        this.power += power;
    }

    /**
     * Gets team.
     *
     * @return the team
     */
    public int getTeam() {
        return team;
    }

    /**
     * Sets team.
     *
     * @param team the team
     */
    public void setTeam(int team) {
        this.team = team;
    }

    public void mark(){
        this.markOfDeath = true;
    }

    public boolean isMarked(){
        return markOfDeath;
    }

}
