package com.andrewjb.gons.objects;

/**
 * Class type Cube.
 */
public class CubeCoord {

    public static final CubeCoord UP_RIGHT   = new CubeCoord(1, 0, -1);
    public static final CubeCoord RIGHT      = new CubeCoord(1, -1, 0);
    public static final CubeCoord DN_RIGHT   = new CubeCoord(0, -1, 1);
    public static final CubeCoord DN_LEFT    = new CubeCoord(-1, 0, 1);
    public static final CubeCoord LEFT       = new CubeCoord(-1, 1, 0);
    public static final CubeCoord UP_LEFT    = new CubeCoord(0, 1, -1);

    public int x;
    public int y;
    public int z;

    /**
     * Scanner cube coord.
     *
     * @param i the
     * @return the cube coord
     */
    public static CubeCoord scanAdjacent(int i) {
        switch(i) {
            case 0: return UP_RIGHT;
            case 1: return RIGHT;
            case 2: return DN_RIGHT;
            case 3: return DN_LEFT;
            case 4: return LEFT;
            case 5: return UP_LEFT;
        }
        return null;
    }

    public boolean hasSameCoordsAs(CubeCoord coord) {
        if (x == coord.x && y == coord.y && z == coord.z)
            return true;
        else
            return false;
    }

    public void setSameAs(CubeCoord coord) {
        x = coord.x;
        y = coord.y;
        z = coord.z;
    }

    public void translate(CubeCoord coord) {
        x += coord.x;
        y += coord.y;
        z += coord.z;
    }

    /**
     * Instantiates a new Cube.
     */
    public CubeCoord() {
        this(0, 0, 0);
    }

    /**
     * Instantiates a new Cube.
     *
     * @param c the c
     */
    public CubeCoord(CubeCoord c) {
        this(c.x, c.y, c.z);
    }

    /**
     * Instantiates a new Cube.
     *
     * @param x the x
     * @param y the y
     * @param z the z
     */
    public CubeCoord(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Gets x.
     *
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * Gets y.
     *
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * Gets z.
     *
     * @return the z
     */
    public int getZ() {
        return z;
    }

    /**
     * Gets location.
     *
     * @return the location
     */
    public CubeCoord getLocation() {
        return new CubeCoord(x, y, z);
    }

    /**
     * Sets location.
     *
     * @param c the c
     */
    public void setLocation(CubeCoord c) {
        setLocation(c.x, c.y, c.z);
    }

    /**
     * Sets location.
     *
     * @param x the x
     * @param y the y
     * @param z the z
     */
    public void setLocation(int x, int y, int z) {
        move(x, y, z);
    }

    /**
     * Sets location.
     *
     * @param x the x
     * @param y the y
     * @param z the z
     */
    public void setLocation(double x, double y, double z) {
        this.x = (int) Math.floor(x+0.5);
        this.y = (int) Math.floor(y+0.5);
        this.z = (int) Math.floor(y+0.5);
    }

    /**
     * Move.
     *
     * @param x the x
     * @param y the y
     * @param z the z
     */
    public void move(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Translate.
     *
     * @param dx the dx
     * @param dy the dy
     * @param dz the dz
     */
    public void translate(int dx, int dy, int dz) {
        this.x += dx;
        this.y += dy;
        this.z += dz;
    }

    /**
     * Sets x.
     *
     * @param x the x
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Sets y.
     *
     * @param y the y
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * Sets z.
     *
     * @param z the z
     */
    public void setZ(int z) {
        this.z = z;
    }
}
