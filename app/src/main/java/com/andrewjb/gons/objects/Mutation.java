package com.andrewjb.gons.objects;

public class Mutation {

    public int power;
    public int team;

    public Mutation(int power, int team) {
        this.power = power;
        this.team = team;
    }
}
