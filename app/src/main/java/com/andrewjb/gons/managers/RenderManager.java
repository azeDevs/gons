package com.andrewjb.gons.managers;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;

import com.andrewjb.gons.objects.Cell;
import com.andrewjb.gons.objects.CubeCoord;
import com.andrewjb.gons.objects.CellGrid;
import com.andrewjb.gons.providers.DrawPath;
import com.andrewjb.gons.providers.PaintProvider;

public class RenderManager {

    public static final String TAG = "RenderManager";

    private static final int DARK = 0;
    private static final int NORM = 1;
    private static final int LGHT = 2;
    private static final int HIGH = 3;

    private static final int CORNER_X = 0;
    private static final int CORNER_Y = 1;
    private static final int CORNER_Z = 2;

    private static final float INNER_SCALING = 0.6f;


    private Point canvasCenter;
    private Paint hexPaint, txtPaint;
    private int x, y, z;
    private int infoLevel;

    public CellGrid list;
    public PaintProvider paint;
    public Point translation;
    public float radius;

    public RenderManager(Context context, Point canvasCenter) {
        this.canvasCenter = canvasCenter;
        this.paint = new PaintProvider(context);
        this.translation = new Point();
        this.list = new CellGrid();
        mockCellList();
    }

    /**
     *   render()
     *   ...
     */
    public void render(Canvas canvas, float unit, float zoom) {
        setInfoLevel(zoom);
        radius = unit + zoom;

        for (int i = 0; i < list.size(); i++) {
            x = list.get(i).getCubeCoord().getX();
            y = list.get(i).getCubeCoord().getY();
            z = list.get(i).getCubeCoord().getZ();

            Point center = getHexagonCenter(canvasCenter);
            hexPaint = getTeamPaint(list.get(i).getTeam(), NORM);
            txtPaint = getTeamPaint(list.get(i).getTeam(), DARK);

            DrawPath.hex(center, radius, hexPaint, canvas);
            drawEffects(i, center, canvas);
        }

    }

    /**
     *   getTeamPaint()
     *   ...
     */
    private Paint getTeamPaint(int team, int shade) {
        switch (team) {
            case Cell.TEAM_RED:
                return paint.red[shade];
            case Cell.TEAM_GRN:
                return paint.grn[shade];
            case Cell.TEAM_NIL:
                if (shade == NORM) return getVacancyPatternPaint();
        }
        return paint.grd[HIGH];
    }

    /**
     *   drawEffects()
     *   ...
     */
    private void drawEffects(int i, Point center, Canvas canvas) {
        // Cell effects
        if (list.get(i).getTeam() != Cell.TEAM_NIL) {
            if (infoLevel >= 1) { // Draw inner hex
                DrawPath.hex(center, radius * INNER_SCALING, txtPaint, canvas);
            }
            if (infoLevel >= 2) { // Draw power amount
                final String powerAmount = String.valueOf(list.get(i).getPower());
                DrawPath.txt(center, radius * INNER_SCALING, hexPaint, canvas, powerAmount);
            }
            if (infoLevel >= 4) { // Draw CubeCoord values
                DrawPath.txt(getEffectLocation(x, y, z, CORNER_X), radius/4, txtPaint, canvas, String.valueOf(x));
                DrawPath.txt(getEffectLocation(x, y, z, CORNER_Y), radius/4, txtPaint, canvas, String.valueOf(y));
                DrawPath.txt(getEffectLocation(x, y, z, CORNER_Z), radius/4, txtPaint, canvas, String.valueOf(z));
            }
        }

        // Vacancy effects
        else if (list.get(i).getTeam() == Cell.TEAM_NIL) {
            if (infoLevel >= 4) { // Draw patternInt
//                final String patternInt = String.valueOf((((x%3) + (y*2)) + 3000) % 3);
//                DrawPath.txt(center, radius/2, txtPaint, canvas, patternInt);
                final String powerAmount = String.valueOf(list.get(i).getPower());
                DrawPath.txt(center, radius/2, txtPaint, canvas, powerAmount);
            }
            if (infoLevel >= 4) { // Draw CubeCoord values
                DrawPath.txt(getEffectLocation(x, y, z, CORNER_X), radius/4, txtPaint, canvas, String.valueOf(x));
                DrawPath.txt(getEffectLocation(x, y, z, CORNER_Y), radius/4, txtPaint, canvas, String.valueOf(y));
                DrawPath.txt(getEffectLocation(x, y, z, CORNER_Z), radius/4, txtPaint, canvas, String.valueOf(z));
            }
            else if (infoLevel >= 3) { // Draw CubeCoord dimensions
                DrawPath.txt(getEffectLocation(x, y, z, CORNER_X), radius / 4, txtPaint, canvas, "x");
                DrawPath.txt(getEffectLocation(x, y, z, CORNER_Y), radius / 4, txtPaint, canvas, "y");
                DrawPath.txt(getEffectLocation(x, y, z, CORNER_Z), radius / 4, txtPaint, canvas, "z");
            }
        }
    }

    /**
     *   getEffectLocation()
     *   ...
     */
    private Point getEffectLocation(int x, int y, int z, int location) {
        Point effectCenter = new Point();
        switch (location) {
            case CORNER_X:
                effectCenter.x = (int) (getHexagonCenter(canvasCenter).x + (radius * 0.69));
                effectCenter.y = (int) (getHexagonCenter(canvasCenter).y - (radius * 0.36));
                break;
            case CORNER_Y:
                effectCenter.x = (int) (getHexagonCenter(canvasCenter).x - (radius * 0.69));
                effectCenter.y = (int) (getHexagonCenter(canvasCenter).y - (radius * 0.36));
                break;
            case CORNER_Z:
                effectCenter.x = (int) (getHexagonCenter(canvasCenter).x);
                effectCenter.y = (int) (getHexagonCenter(canvasCenter).y + (radius * 0.78));
                break;
        }
        return effectCenter;
    }

    /**
     *   getVacancyPatternPaint()
     *   ...
     */
    private Paint getVacancyPatternPaint() {
        int pattern;
        pattern = (((x %3) + (y *2)) + 3000) % 3;
        switch (pattern) {
            case 0: return paint.grd[LGHT];
            case 1: return paint.grd[NORM];
            case 2: return paint.grd[DARK];
        }
        return paint.bkg[NORM];
    }

    /**
     *   getVacancyPatternPaint()
     *   ...
     */
    private Point getHexagonCenter(Point canvasCenter) {
        Point hexCenter = new Point();
        int modX = x - y;
        int modY = z;
        hexCenter.x = (int) ((Math.sqrt(3) / 2 * (radius)) * modX) + canvasCenter.x + translation.x;
        hexCenter.y = (int) ((radius * 2 * 3 / 4) * modY) + canvasCenter.y + translation.y;

        return new Point(hexCenter.x, hexCenter.y);
    }

    /**
     *   setInfoLevel()
     *   ...
     */
    private void setInfoLevel(float zoom) {
                         infoLevel = 0;
        if (zoom >= -20) infoLevel = 1;
        if (zoom >= -10) infoLevel = 2;
        if (zoom >=  10) infoLevel = 3;
        if (zoom >=  30) infoLevel = 4;
    }


    public void mockCellList() {
        list.clear();
        list.addCell(new CubeCoord( 0,  0,  0), 0, 0x000);

        list.addCell(new CubeCoord( 1,  0, -1), 2, Cell.TEAM_RED);
        list.addCell(new CubeCoord( 1, -1,  0), 2, Cell.TEAM_RED);
        list.addCell(new CubeCoord( 0, -1,  1), 2, Cell.TEAM_RED);
        list.addCell(new CubeCoord(-1,  0,  1), 2, Cell.TEAM_GRN);
        list.addCell(new CubeCoord(-1,  1,  0), 2, Cell.TEAM_GRN);
        list.addCell(new CubeCoord( 0,  1, -1), 2, Cell.TEAM_GRN);

        list.addCell(new CubeCoord( 2,  0, -2), 2, Cell.TEAM_RED);
        list.addCell(new CubeCoord( 2, -1, -1), 2, Cell.TEAM_RED);
        list.addCell(new CubeCoord( 2, -2,  0), 2, Cell.TEAM_RED);
        list.addCell(new CubeCoord( 1, -2,  1), 2, Cell.TEAM_RED);
        list.addCell(new CubeCoord( 0, -2,  2), 2, Cell.TEAM_RED);
        list.addCell(new CubeCoord(-1, -1,  2), 2, Cell.TEAM_RED);

        list.addCell(new CubeCoord(-2,  0,  2), 2, Cell.TEAM_GRN);
        list.addCell(new CubeCoord(-2,  1,  1), 2, Cell.TEAM_GRN);
        list.addCell(new CubeCoord(-2,  2,  0), 2, Cell.TEAM_GRN);
        list.addCell(new CubeCoord(-1,  2, -1), 2, Cell.TEAM_GRN);
        list.addCell(new CubeCoord( 0,  2, -2), 2, Cell.TEAM_GRN);
        list.addCell(new CubeCoord( 1,  1, -2), 2, Cell.TEAM_GRN);

        list.addCell(new CubeCoord( 3,  0, -3), 0, 0x000);
        list.addCell(new CubeCoord( 3, -1, -2), 0, 0x000);
        list.addCell(new CubeCoord( 3, -2, -1), 0, 0x000);
        list.addCell(new CubeCoord( 3, -3,  0), 0, 0x000);
        list.addCell(new CubeCoord( 2, -3,  1), 0, 0x000);
        list.addCell(new CubeCoord( 1, -3,  2), 0, 0x000);

        list.addCell(new CubeCoord( 0, -3,  3), 0, 0x000);
        list.addCell(new CubeCoord(-1, -2,  3), 0, 0x000);
        list.addCell(new CubeCoord(-2, -1,  3), 0, 0x000);
        list.addCell(new CubeCoord(-3,  0,  3), 0, 0x000);
        list.addCell(new CubeCoord(-3,  1,  2), 0, 0x000);
        list.addCell(new CubeCoord(-3,  2,  1), 0, 0x000);

        list.addCell(new CubeCoord(-3,  3,  0), 0, 0x000);
        list.addCell(new CubeCoord(-2,  3, -1), 0, 0x000);
        list.addCell(new CubeCoord(-1,  3, -2), 0, 0x000);
        list.addCell(new CubeCoord( 0,  3, -3), 0, 0x000);
        list.addCell(new CubeCoord( 1,  2, -3), 0, 0x000);
        list.addCell(new CubeCoord( 2,  1, -3), 0, 0x000);
    }

//    public void mockCellList() {
//        list.clear();
//        list.addCell(new CubeCoord( 0,  0,  0), 0, 0x000);
//
//        list.addCell(new CubeCoord( 1,  0, -1), 0, 0x000);
//        list.addCell(new CubeCoord( 1, -1,  0), 0, 0x000);
//        list.addCell(new CubeCoord( 0, -1,  1), 0, 0x000);
//        list.addCell(new CubeCoord(-1,  0,  1), 0, 0x000);
//        list.addCell(new CubeCoord(-1,  1,  0), 0, 0x000);
//        list.addCell(new CubeCoord( 0,  1, -1), 0, 0x000);
//
//        list.addCell(new CubeCoord( 2,  0, -2), 0, 0x000);
//        list.addCell(new CubeCoord( 2, -1, -1), 0, 0x000);
//        list.addCell(new CubeCoord( 2, -2,  0), 0, 0x000);
//        list.addCell(new CubeCoord( 1, -2,  1), 0, 0x000);
//        list.addCell(new CubeCoord( 0, -2,  2), 0, 0x000);
//        list.addCell(new CubeCoord(-1, -1,  2), 0, 0x000);
//
//        list.addCell(new CubeCoord(-2,  0,  2), 0, 0x000);
//        list.addCell(new CubeCoord(-2,  1,  1), 0, 0x000);
//        list.addCell(new CubeCoord(-2,  2,  0), 0, 0x000);
//        list.addCell(new CubeCoord(-1,  2, -1), 0, 0x000);
//        list.addCell(new CubeCoord( 0,  2, -2), 0, 0x000);
//        list.addCell(new CubeCoord( 1,  1, -2), 0, 0x000);
//
//        list.addCell(new CubeCoord( 3,  0, -3), 0, 0x000);
//        list.addCell(new CubeCoord( 3, -1, -2), 0, 0x000);
//        list.addCell(new CubeCoord( 3, -2, -1), 0, 0x000);
//        list.addCell(new CubeCoord( 3, -3,  0), 0, 0x000);
//        list.addCell(new CubeCoord( 2, -3,  1), 0, 0x000);
//        list.addCell(new CubeCoord( 1, -3,  2), 0, 0x000);
//
//        list.addCell(new CubeCoord( 0, -3,  3), 0, 0x000);
//        list.addCell(new CubeCoord(-1, -2,  3), 0, 0x000);
//        list.addCell(new CubeCoord(-2, -1,  3), 0, 0x000);
//        list.addCell(new CubeCoord(-3,  0,  3), 0, 0x000);
//        list.addCell(new CubeCoord(-3,  1,  2), 0, 0x000);
//        list.addCell(new CubeCoord(-3,  2,  1), 0, 0x000);
//
//        list.addCell(new CubeCoord(-3,  3,  0), 0, 0x000);
//        list.addCell(new CubeCoord(-2,  3, -1), 0, 0x000);
//        list.addCell(new CubeCoord(-1,  3, -2), 0, 0x000);
//        list.addCell(new CubeCoord( 0,  3, -3), 0, 0x000);
//        list.addCell(new CubeCoord( 1,  2, -3), 0, 0x000);
//        list.addCell(new CubeCoord( 2,  1, -3), 0, 0x000);
//
//        list.addCell(new CubeCoord( 4, -1, -3), 1, 0x0F0);
//        list.addCell(new CubeCoord( 4, -2, -2), 2, 0x0F0);
//        list.addCell(new CubeCoord( 4, -3, -1), 3, 0x0F0);
//        list.addCell(new CubeCoord( 5, -3, -2), 2, 0x0F0);
//        list.addCell(new CubeCoord( 5, -2, -3), 1, 0x0F0);
//
//        list.addCell(new CubeCoord(-4,  1,  3), 1, 0xF00);
//        list.addCell(new CubeCoord(-4,  2,  2), 2, 0xF00);
//        list.addCell(new CubeCoord(-4,  3,  1), 3, 0xF00);
//        list.addCell(new CubeCoord(-5,  3,  2), 2, 0xF00);
//        list.addCell(new CubeCoord(-5,  2,  3), 1, 0xF00);
//    }

}
