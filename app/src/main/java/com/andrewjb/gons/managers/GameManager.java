package com.andrewjb.gons.managers;

import android.content.Context;

import com.andrewjb.gons.views.CanvasView;
import com.andrewjb.gons.views.DebugView;
import com.andrewjb.gons.views.GameView;

/**
 * Class type GameManager.
 */
public class GameManager {

    public static final String TAG = "GameManager";


    private RenderManager renderManager;
    private DebugView debugView;
    private GameView gameView;
    private CanvasView canvasView;

    /**
     * Instantiates a new Game manager.
     */
    public GameManager(Context context) {
        this.gameView = new GameView();
        this.canvasView = gameView.getCanvasView();
        this.debugView = gameView.getDebugView();
        this.renderManager = gameView.getRenderManager();
    }

    /**
     * New game view game view.
     *
     * @return the game view
     */
    public GameView newGameView() {
        return this.gameView;
    }

}
