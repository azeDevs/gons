package com.andrewjb.gons.providers;

import android.content.Context;
import android.graphics.Paint;

import com.andrewjb.gons.R;

/**
 * Class type PaintProvider.
 */
public class PaintProvider {

    public static final String TAG = "PaintProvider";

    public static final int SOLID = 0;
    public static final int WIREFRAME = 1;


    private Context context;

    public Paint[] bkg, grd, red, grn;

    /**
     * Instantiates a new Paint provider.
     *
     * @param context the context
     */
    public PaintProvider(Context context) {
        this.context = context;
        this.bkg = new Paint[4];
        this.grd = new Paint[4];
        this.red = new Paint[4];
        this.grn = new Paint[4];
        setStyles(SOLID);
    }

    public void setStyles(int style) {
        stylePaints(bkg, style);
        stylePaints(grd, style);
        stylePaints(red, style);
        stylePaints(grn, style);
        setColors();
    }

    private void setColors() {
        bkg[0].setColor(context.getResources().getColor(R.color.bkg0));
        bkg[1].setColor(context.getResources().getColor(R.color.bkg1));
        bkg[2].setColor(context.getResources().getColor(R.color.bkg2));
        bkg[3].setColor(context.getResources().getColor(R.color.bkg3));

        grd[0].setColor(context.getResources().getColor(R.color.grd0));
        grd[1].setColor(context.getResources().getColor(R.color.grd1));
        grd[2].setColor(context.getResources().getColor(R.color.grd2));
        grd[3].setColor(context.getResources().getColor(R.color.grd3));

        red[0].setColor(context.getResources().getColor(R.color.red0));
        red[1].setColor(context.getResources().getColor(R.color.red1));
        red[2].setColor(context.getResources().getColor(R.color.red2));
        red[3].setColor(context.getResources().getColor(R.color.red3));

        grn[0].setColor(context.getResources().getColor(R.color.grn0));
        grn[1].setColor(context.getResources().getColor(R.color.grn1));
        grn[2].setColor(context.getResources().getColor(R.color.grn2));
        grn[3].setColor(context.getResources().getColor(R.color.grn3));
    }

    private void stylePaints(Paint[] paints, int style) {
        for (int n=0; n<paints.length; n++) {
            paints[n] = new Paint();
            paints[n].setAntiAlias(true);
            switch (style) {
                case SOLID:
                    paints[n].setStyle(Paint.Style.FILL);
                    break;
                case WIREFRAME:
                    paints[n].setStyle(Paint.Style.STROKE);
                    break;
            }
        }
    }

}
