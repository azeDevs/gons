package com.andrewjb.gons.providers;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;

/**
 * Class type DrawPath.
 */
public class DrawPath {

    /**
     * Hexagon.
     *
     * @param center   the center
     * @param radius   the radius
     * @param hexPaint the hex paint
     * @param canvas   the canvas
     */
    public static void hex(Point center, float radius, Paint hexPaint, Canvas canvas) {
        Point[] points = new Point[6];

        for (int i=0; i<6; i++) {
            float angle_deg = 60 * i + 30;
            float angle_rad = (float) Math.PI / 180 * angle_deg;
            points[i] = new Point();
            points[i].x = (int) (center.x + radius * (float) Math.cos(angle_rad));
            points[i].y = (int) (center.y + radius * (float) Math.sin(angle_rad));
        }

        canvas.drawPath(getPath(points), hexPaint);
    }

    private static Path getPath(Point[] point) {
        Path path = new Path();
        path.reset();
        path.moveTo(point[0].x, point[0].y);
        for (int i = 1; i < point.length; i++)
            path.lineTo(point[i].x, point[i].y);
        path.lineTo(point[0].x, point[0].y);
        return path;
    }

    /**
     * Text.
     *
     * @param center   the center
     * @param radius   the radius
     * @param txtPaint the txt paint
     * @param canvas   the canvas
     * @param text     the txt
     */
    public static void txt(Point center, float radius, Paint txtPaint, Canvas canvas, String text) {
        txtPaint.setTextSize(radius);
        txtPaint.setFakeBoldText(true);
        txtPaint.setTextAlign(Paint.Align.CENTER);
        canvas.drawText(text, center.x, center.y + (radius / 3), txtPaint);
    }

}