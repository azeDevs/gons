package com.andrewjb.gons;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.andrewjb.gons.managers.GameManager;
import com.bluelinelabs.conductor.Conductor;
import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;

/**
 * Class type MainActivity.
 */
public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";
    

    private Router mainRouter;
    private ViewGroup mainContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        removeBars();
        setContentView(R.layout.activity_main);

        mainContainer = (ViewGroup) findViewById(R.id.controller_container);
        mainRouter = Conductor.attachRouter(this, mainContainer, savedInstanceState);
        mainRouter.pushController(RouterTransaction.with(new GameManager(getApplicationContext()).newGameView()));

    }

    private void removeBars() {
        // Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        // Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();
    }

}
