package com.andrewjb.gons.views;

import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andrewjb.gons.R;
import com.andrewjb.gons.managers.RenderManager;
import com.andrewjb.gons.providers.PaintProvider;
import com.jakewharton.rxbinding.view.RxView;

/**
 * Class type DebugView.
 */
public class DebugView extends LinearLayout {

    public static final String TAG = "DebugView";


    private CanvasView canvas;
    private RenderManager renderManager;

    private TextView txtScaling;
    private TextView txtZoom;
    private TextView btnZoomin;
    private TextView btnZoomout;
    private TextView btnResetZoom;

    private TextView txtTranslationX;
    private TextView txtTranslationY;
    private TextView btnResetTranslation;

    private TextView txtCanvasUnit;
    private TextView txtCanvasWidth;
    private TextView txtCanvasHeight;
    private TextView btnSolidMode;
    private TextView btnWireMode;

    private TextView txtTerritory;
    private TextView btnGenerate;
    private TextView btnResetGrid;


    public DebugView(Context context) {
        super(context);
        init();
    }

    public DebugView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DebugView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.view_debug, this);

        this.txtScaling = (TextView)findViewById(R.id.scaling);
        this.txtZoom = (TextView)findViewById(R.id.zoom);
        this.btnZoomin = (TextView) findViewById(R.id.btn_zoomin);
        this.btnZoomout = (TextView) findViewById(R.id.btn_zoomout);
        this.btnResetZoom = (TextView) findViewById(R.id.btn_reset_zoom);

        this.txtTranslationX = (TextView)findViewById(R.id.translation_x);
        this.txtTranslationY = (TextView)findViewById(R.id.translation_y);
        this.btnResetTranslation = (TextView) findViewById(R.id.btn_reset_translation);

        this.txtCanvasWidth = (TextView)findViewById(R.id.canvas_width);
        this.txtCanvasHeight = (TextView)findViewById(R.id.canvas_height);
        this.txtCanvasUnit = (TextView)findViewById(R.id.canvas_unit);
        this.btnSolidMode = (TextView)findViewById(R.id.btn_solid_mode);
        this.btnWireMode = (TextView)findViewById(R.id.btn_wire_mode);

        this.txtTerritory = (TextView)findViewById(R.id.list_territory);
        this.btnGenerate = (TextView)findViewById(R.id.btn_generate);
        this.btnResetGrid = (TextView)findViewById(R.id.btn_reset_grid);

        RxView.clicks(btnZoomin).subscribe(aVoid -> debugZoom((int) (canvas.unit/2.66)));
        RxView.clicks(btnZoomout).subscribe(aVoid -> debugZoom((int) -(canvas.unit/2.66)));
        RxView.clicks(btnResetZoom).subscribe(aVoid -> debugZoom(0));
        RxView.clicks(btnSolidMode).subscribe(aVoid -> changeStyleMode(PaintProvider.SOLID));
        RxView.clicks(btnWireMode).subscribe(aVoid -> changeStyleMode(PaintProvider.WIREFRAME));
        RxView.clicks(btnResetTranslation).subscribe(aVoid -> animateResetTranslation());
        RxView.clicks(btnGenerate).subscribe(aVoid -> gridGenerate());
        RxView.clicks(btnResetGrid).subscribe(aVoid -> gridReset());
    }

    private void changeStyleMode(int style) {
        renderManager.paint.setStyles(style);
        canvas.invalidate();
    }

    private void gridGenerate() {
        renderManager.list.generate();
        canvas.invalidate();
    }

    private void gridReset() {
        renderManager.mockCellList();
        canvas.invalidate();
    }

    /**
     * Slide debug.
     */
    public void showDebugMenu() {
        ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) this.getLayoutParams();
        ValueAnimator anim = ValueAnimator.ofInt(p.rightMargin, (p.rightMargin == 0 ? -p.width : 0));
        anim.addUpdateListener(animation -> { p.setMargins(p.leftMargin, p.topMargin, (Integer) animation.getAnimatedValue(), p.bottomMargin); this.requestLayout(); });
        anim.setDuration(500);
        anim.start();
    }

    /**
     * Debug zoom.
     *
     * @param amount the amount
     */
    public void debugZoom(int amount) {
        ValueAnimator anim = ValueAnimator.ofFloat(canvas.zoom, amount == 0 ? canvas.zoom - canvas.zoom : canvas.zoom + amount);
        anim.addUpdateListener(animation -> {
            canvas.zoom = (float) animation.getAnimatedValue();
            canvas.invalidate();
        });
        anim.setDuration(500);
        anim.start();
    }

    /**
     * Reset translation.
     */
    public void animateResetTranslation() {
        ValueAnimator animX = ValueAnimator.ofInt(renderManager.translation.x, 0);
        animX.addUpdateListener(animation -> {
            renderManager.translation.x = (int) animation.getAnimatedValue();
            canvas.invalidate();
        });
        animX.setInterpolator(new DecelerateInterpolator());
        animX.setDuration(500);
        animX.start();

        ValueAnimator animY = ValueAnimator.ofInt(renderManager.translation.y, 0);
        animY.addUpdateListener(animation -> {
            renderManager.translation.y = (int) animation.getAnimatedValue();
            canvas.invalidate();
        });
        animY.setInterpolator(new DecelerateInterpolator());
        animY.setDuration(500);
        animY.start();
    }

    /**
     * Update.
     */
    public void update() {
        txtScaling.setText((int) canvas.unit + " + " + (int) canvas.zoom + " = " + (int) renderManager.radius);
        txtZoom.setText("x" + Math.ceil(((canvas.unit + canvas.zoom) / canvas.unit) * 100) / 100);

        txtTranslationX.setText("x = " + renderManager.translation.x);
        txtTranslationY.setText("y = " + renderManager.translation.y);

        txtCanvasWidth.setText("width = " + (int) canvas.width);
        txtCanvasHeight.setText("height = " + (int) canvas.height);
        txtCanvasUnit.setText("px/unit = " + (int) canvas.unit);

        txtTerritory.setText("territory = " + (int) renderManager.list.size());
    }

    public void setCanvas(CanvasView canvas) {
        this.canvas = canvas;
    }

    public void setRenderManager(RenderManager renderManager) {
        this.renderManager = renderManager;
    }

}
