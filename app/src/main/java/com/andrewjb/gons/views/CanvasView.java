package com.andrewjb.gons.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.MotionEvent;
import android.view.View;

import com.andrewjb.gons.managers.RenderManager;

public class CanvasView extends View {

    public static final String TAG = "CanvasView";

    private static final float DEFAULT_SCALE_REDUCTION = 0.05f;
    private static final float DEFAULT_TOUCH_REDUCTION = 0.85f;


    private RenderManager renderManager;

    public Point center;
    public float width;
    public float height;
    public float unit;
    public float zoom;

    private Point p1;
    private Point p2;
    private int origTranslationX;
    private int origTranslationY;

    public CanvasView(Context context) {
        super(context);
        init();
    }

    public CanvasView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CanvasView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        this.center = new Point();
        this.p1 = new Point();
        this.p2 = new Point();
        this.zoom = 0f;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        refreshRenderMods();
        renderManager.render(canvas, unit, zoom);
    }

    private void refreshRenderMods() {
        width = getWidth();
        height = getHeight();
        center.x = (int) (width /2);
        center.y = (int) (height /2);
        unit = (getHeight() * DEFAULT_SCALE_REDUCTION);
        // Minimum zoom
        if (zoom < -(unit / 4) * 3)
            zoom = -(unit / 4) * 3;
        // Maximum zoom
        if (zoom > unit * 3)
            zoom = unit * 3;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        invalidate();
        Point hexCenter = new Point();

        //TODO: Get this dynamically from each hex, attach the hex's id to it?
        hexCenter.x = (int) (Math.sqrt(3)/2 * renderManager.radius) * 0 + center.x + renderManager.translation.x;
        hexCenter.y = (int) (renderManager.radius * 2 * 3 / 4) * 0 + center.y + renderManager.translation.y;

        int x = (int)event.getX();
        int y = (int)event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                origTranslationX = renderManager.translation.x;
                origTranslationY = renderManager.translation.y;
                p1.x = x;
                p1.y = y;

                Log.i(TAG, "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━");
                Log.i(TAG, "TRANSLATION x"+renderManager.translation.x+" y"+renderManager.translation.y);
                Log.i(TAG, "     ORIGIN x"+origTranslationX+" y"+origTranslationY);
                Log.i(TAG, "       DOWN x"+p1.x+" y"+p1.y);

                //TODO: Make this detect from any possible hex position based on zoom and renderManager.translation
                if (Math.pow(x-hexCenter.x, 2) + Math.pow(y-hexCenter.y, 2) <= Math.pow(renderManager.radius * DEFAULT_TOUCH_REDUCTION, 2)) {
                    this.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
//                    Toast.makeText(getContext(), "0, 0, 0", Toast.LENGTH_SHORT).show();
                    Log.i(TAG, "ACTION_DOWN renderManager.radius "+(int) (Math.pow(x-hexCenter.x, 2) + Math.pow(y-hexCenter.y, 2)));
                    Log.i(TAG, " ┗━━▶ LIMIT renderManager.radius "+ (int) Math.pow(renderManager.radius * DEFAULT_TOUCH_REDUCTION, 2));
                }

                Log.i(TAG, "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━");
                break;

            case MotionEvent.ACTION_MOVE:
                p2.x = x;
                p2.y = y;
//                Log.i(TAG, "DRAGGING x"+p1.x+"-"+p2.x+"="+(p1.x - p2.x)+" y"+p1.y+"-"+p2.y+"="+(p1.y - p2.y));
                renderManager.translation.x = origTranslationX-(p1.x - p2.x);
                renderManager.translation.y = origTranslationY-(p1.y - p2.y);
                break;

            case MotionEvent.ACTION_UP:
                Log.i(TAG, "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━");
                Log.i(TAG, "TRANSLATION x"+renderManager.translation.x+" y"+renderManager.translation.y);
                Log.i(TAG, "         UP x"+p2.x+" y"+p2.y);
                Log.i(TAG, "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━");
                break;

        }
        return true;
    }


    public void setRenderManager(RenderManager renderManager) {
        this.renderManager = renderManager;
    }


}
