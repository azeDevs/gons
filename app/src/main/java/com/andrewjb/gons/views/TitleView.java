package com.andrewjb.gons.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.andrewjb.gons.R;

/**
 * Class type TitleView.
 */
public class TitleView extends LinearLayout {

    public TitleView(Context context) {
        super(context);
        init();
    }

    public TitleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TitleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.view_title, this);
    }

}
