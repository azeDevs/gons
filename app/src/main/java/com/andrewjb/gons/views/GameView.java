package com.andrewjb.gons.views;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andrewjb.gons.databinding.ViewGameBinding;
import com.andrewjb.gons.managers.RenderManager;
import com.bluelinelabs.conductor.Controller;
import com.jakewharton.rxbinding.view.RxView;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Class type GameView.
 */
public class GameView extends Controller {

    public static final String TAG = "GameView";


    private ViewGameBinding binding;

    private RenderManager renderManager;
    private DebugView debugView;
    private CanvasView canvasView;
    private View titleButton;
    private Timer t;

    @NonNull
    @Override
    protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        binding = ViewGameBinding.inflate(inflater);

        debugView = binding.panelDebug;
        canvasView = binding.panelGame;
        titleButton = binding.titleButton;

        renderManager = new RenderManager(getApplicationContext(), canvasView.center);

        canvasView.setRenderManager(renderManager);

        createDebugMenu();

        return binding.getRoot();
    }

    private void createDebugMenu() {
        debugView.setCanvas(canvasView);
        debugView.setRenderManager(renderManager);
        RxView.clicks(titleButton).subscribe(aVoid -> debugView.showDebugMenu());
        debugView.showDebugMenu();
    }

    @Override
    protected void onActivityPaused(Activity activity) {
        super.onActivityPaused(activity);
        t.cancel();
    }

    @Override
    protected void onActivityResumed(Activity activity) {
        super.onActivityResumed(activity);
        createUpdateTimer();
    }

    private void createUpdateTimer() {
        t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(
                        () -> debugView.update()
                );
            }
        }, 50, 50);
    }

    public DebugView getDebugView() {
        return debugView;
    }

    public CanvasView getCanvasView() {
        return canvasView;
    }

    public RenderManager getRenderManager() {
        return renderManager;
    }

}
