# gons 

Currently gons is an Android framework meant to render and store hexagonal information for games or simulations.

![gons](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Regular_polygon_6_annotated.svg/220px-Regular_polygon_6_annotated.svg.png)

[Documentation](https://ajbtee.github.io/gons/)


## Future Goals

* Build example game using gons framework
* Expand framework into a proper open-source library
* Add game development features beyond hex grid management and canvas rendering


## Game of Natural Selection

The game is played on a hexagonal *grid*. A *grid* space can contain a *cell*. A *cell's* *power* can range from 1 to 6. If a *cell's* *power* is reduced to 0, it is destroyed. When the game's *grid* is first created, a preset starting *cell* configuration is added to it.

#### Updating Cell Power
```
A cell's state is updated everytime a generation occurs.
The cell's power is governed by the number of neighboring *cells*.
```
| Neighbors | Power Change  |
| --------: |:--------------|
| 1         | -2            |
| 2         | -1            |
| 3         | Stabilized    |
| 4         | Stabilized    |
| 5         | +1            |
| 6         | +1            |
| 7         | +1            |
| 8         | +2            |
| 9         | +2            |
| 10        | +2            |
| 11        | +2            |
| 12        | +2            |

#### Updating Grid Vacancies
```
A cell's power governs the range of empty grid space that are visible near it.
Empty grid spaces are generated/regenerated every generation.
This effect serves aesthetic, primarily.
```
| Cell Power | Grid Added |
| ---------: |:-----------|
| 0          | 0          |
| 1          | 1          |
| 2          | 1          |
| 3          | 2          |
| 4          | 2          |
| 5          | 2          |
| 6          | 3          |

#### Miscellaneous
* A random 3 character hexadecimal tag is assigned to the player when they first begin.
* New players joining the game are attached to a nearby grid vacancy.


## Library Acknowledgments

* RxJava
* Conductor
* Dagger